import math
import numpy as np

# --- FUNCTIONS AND CLASSES ----------------------------------------------------
class hmm:
    def __init__(self, init_probs, trans_probs, emission_probs):
        self.init_probs = init_probs
        self.trans_probs = trans_probs
        self.emission_probs = emission_probs

def translate_observations_to_indices(obs):
    mapping = {'a': 0, 'c': 1, 'g': 2, 't': 3}
    return [mapping[symbol.lower()] for symbol in obs]

def translate_indices_to_observations(indices):
    mapping = ['a', 'c', 'g', 't']
    return ''.join(mapping[idx] for idx in indices)

def translate_path_to_indices(path):
    return list(map(lambda x: int(x), path))

def translate_indices_to_path(indices):
    return ''.join([str(i) for i in indices])

def make_table(m, n):
    """Make a table with `m` rows and `n` columns filled with zeros."""
    return np.zeros([m, n])

def log(x):
    if np.size(x) > 1:
        logx = np.zeros(len(x))
        for i in range(len(x)):
            if x[i] == 0:
                logx[i] = float('-inf')
            else:
                logx[i] = math.log(x[i])
        return logx
    elif x == 0:
        return float('-inf')
    else:
        return math.log(x)

def compute_w_log(model, x):
    K = len(model.init_probs)
    N = len(x)
    w = make_table(K, N)
    w[:,:] = float('-inf')
    # Base case: fill out w[i][0] for i = 0..k-1
    for i in range(0, K-1): #range(K-1)
        w[i, 0] = model.init_probs[i]*model.emission_probs[i, x[0]]
    # Inductive case: fill out w[i][j] for i = 0..k, j = 0..n-1
    for n in range(1, N):
        for k in range(K):
            for j in range(K):
                w[k, n] = np.max([w[k, n], log(model.emission_probs[k, x[n]]) + 
                                           w[j, n-1] + 
                                           log(model.trans_probs[j, k])])
    return w

def opt_path_prob_log(w):
    return np.max(w[:, -1])

def backtrack_log(model, x, w):
    K = len(model.init_probs)
    N = len(x)
    z = np.zeros(N, dtype=int)
    z[-1] = np.argmax(w[:, -1])
    # taking argmax relative to columns (k)
    for n in range(N-2, -1, -1):
        z[n] = np.argmax(log(model.emission_probs[z[n+1], x[n+1]]) + 
                         w[:, n] + 
                         log(model.trans_probs[:, z[n+1]]))
    return z

def count_transitions_and_emissions(Kdim, Ddim, x, z):
    """
    Returns a KxK matrix and a KxD matrix containing counts cf. above
    """
    K = np.zeros([Kdim,Kdim])
    D = np.zeros([Kdim,Ddim])
    N = len(z)
    for n in range(0, N):
        if n<N-1:
            # counting state transitions
            i = z[n]
            j = z[n + 1]
            K[i, j] += 1
        # counting emissions/outputs
        k = x[n]
        D[i, k] += 1
    return K, D

def training_by_counting(Kdim, Ddim, x, z):
    # counting init_params (always deterministic in this HMM model)
    Pi = np.zeros(Kdim)
    Pi[z[0]] = 1
    # counting trans_probs
    K, D = count_transitions_and_emissions(Kdim, Ddim, x, z)
    # adding seudo counts with mask
    K += (K == 0)
    D += (D == 0)
    Ksum = np.sum(K, axis=1)
    Dsum = np.sum(D, axis=1)
    for row in range(Kdim):
        K[row, :] = K[row, :] / Ksum[row]
        D[row, :] = D[row, :] / Dsum[row]
    return hmm(Pi, K, D)

def viterbi_update_model(model, x):
    """
    return a new model that corresponds to one round of Viterbi training, 
    i.e. a model where the parameters reflect training by counting on x 
    and z_vit, where z_vit is the Viterbi decoding of x under the given 
    model.
    """
    # initial model parameters are given in "model", now calculating "w"
    w = compute_w_log(model, x)
    # doing backtracking using new "w"
    z_new = backtrack_log(model, x, w)
    # now doing TbC to update model parameters
    hmm_update = training_by_counting(Kdim = np.shape(model.trans_probs)[0]
                                     ,Ddim = np.shape(model.emission_probs)[1]
                                     ,x = x
                                     ,z = z_new)
    return hmm_update

def viterbi_training(x, z, init_model=None, updates=10, eps=10e-5, modelDims=np.array([None, None])):
    """
    Parameters :
        init_model : initial model parameters of the hmm class
        x : outputs of the hmm
        z : known latent states in the hmm
        updates : max number of viterbi updates before returning
        eps : sensitivity of early stopping
    """
    
    if init_model == None and np.sum(modelDims == None) == 0:
        init_model = training_by_counting(modelDims[0], modelDims[1], x, z)
    elif init_model != None and np.sum(modelDims == None) == 0:
        print("\n Error in viterbi_training input, either in init_model or modelDims")
        return None
    elif init_model == None and np.sum(modelDims == None) != 0:
        print("\n Error in viterbi_training input, either in init_model or modelDims")
        return None

    model = init_model
    update = 0
    while update < updates:
        print("viterbi training update {0}/{1}".format(update+1, updates))
        # run update
        model = viterbi_update_model(model, x)        
        update += 1
    return model

# --- CODE PART 1 --------------------------------------------------------------
init_probs_7_state = np.array([0.00, 0.00, 0.00, 1.00, 0.00, 0.00, 0.00])

trans_probs_7_state = np.array([
    [0.00, 0.00, 0.90, 0.10, 0.00, 0.00, 0.00],
    [1.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00],
    [0.00, 1.00, 0.00, 0.00, 0.00, 0.00, 0.00],
    [0.00, 0.00, 0.05, 0.90, 0.05, 0.00, 0.00],
    [0.00, 0.00, 0.00, 0.00, 0.00, 1.00, 0.00],
    [0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 1.00],
    [0.00, 0.00, 0.00, 0.10, 0.90, 0.00, 0.00],
])

emission_probs_7_state = np.array([
    #   A     C     G     T
    [0.30, 0.25, 0.25, 0.20],
    [0.20, 0.35, 0.15, 0.30],
    [0.40, 0.15, 0.20, 0.25],
    [0.25, 0.25, 0.25, 0.25],
    [0.20, 0.40, 0.30, 0.10],
    [0.30, 0.20, 0.30, 0.20],
    [0.15, 0.30, 0.20, 0.35],
])

# randomly initiated hmm model parameters
init_probs_random     = np.random.randint(0, 10, size=[1, 7]).astype(float)
trans_probs_random    = np.random.randint(0, 200, size=[7, 7]).astype(float)
emission_probs_random = np.random.randint(0, 4, size=[7, 4]).astype(float)

init_sum = np.sum(init_probs_random, axis=1)
trans_sum = np.sum(trans_probs_random, axis=1)
emission_sum = np.sum(emission_probs_random, axis=1)
init_probs_random     = init_probs_random / init_sum
for i in range(7):
    trans_probs_random[i, :]    = trans_probs_random[i, :] / trans_sum[i]
    emission_probs_random[i, :] = emission_probs_random[i, :] / emission_sum[i]

hmm_7_state_random = hmm(init_probs_random, trans_probs_random, emission_probs_random)

hmm_7_state = hmm(init_probs_7_state, trans_probs_7_state, emission_probs_7_state)

x_long = 'TGAGTATCACTTAGGTCTATGTCTAGTCGTCTTTCGTAATGTTTGGTCTTGTCACCAGTTATCCTATGGCGCTCCGAGTCTGGTTCTCGAAATAAGCATCCCCGCCCAAGTCATGCACCCGTTTGTGTTCTTCGCCGACTTGAGCGACTTAATGAGGATGCCACTCGTCACCATCTTGAACATGCCACCAACGAGGTTGCCGCCGTCCATTATAACTACAACCTAGACAATTTTCGCTTTAGGTCCATTCACTAGGCCGAAATCCGCTGGAGTAAGCACAAAGCTCGTATAGGCAAAACCGACTCCATGAGTCTGCCTCCCGACCATTCCCATCAAAATACGCTATCAATACTAAAAAAATGACGGTTCAGCCTCACCCGGATGCTCGAGACAGCACACGGACATGATAGCGAACGTGACCAGTGTAGTGGCCCAGGGGAACCGCCGCGCCATTTTGTTCATGGCCCCGCTGCCGAATATTTCGATCCCAGCTAGAGTAATGACCTGTAGCTTAAACCCACTTTTGGCCCAAACTAGAGCAACAATCGGAATGGCTGAAGTGAATGCCGGCATGCCCTCAGCTCTAAGCGCCTCGATCGCAGTAATGACCGTCTTAACATTAGCTCTCAACGCTATGCAGTGGCTTTGGTGTCGCTTACTACCAGTTCCGAACGTCTCGGGGGTCTTGATGCAGCGCACCACGATGCCAAGCCACGCTGAATCGGGCAGCCAGCAGGATCGTTACAGTCGAGCCCACGGCAATGCGAGCCGTCACGTTGCCGAATATGCACTGCGGGACTACGGACGCAGGGCCGCCAACCATCTGGTTGACGATAGCCAAACACGGTCCAGAGGTGCCCCATCTCGGTTATTTGGATCGTAATTTTTGTGAAGAACACTGCAAACGCAAGTGGCTTTCCAGACTTTACGACTATGTGCCATCATTTAAGGCTACGACCCGGCTTTTAAGACCCCCACCACTAAATAGAGGTACATCTGA'
z_long = '3333321021021021021021021021021021021021021021021021021021021021021021033333333334564564564564564564564564564564564564564564564564564564564564564564564564564564564564564564564564564564564564564564564564564564563210210210210210210210210210210210210210210210210210210210210210210210210210210210210210210210210210210210210210210210210210210210321021021021021021021021033334564564564564564564564564564564564564564564564564564564564564564564564564564564564564564564564564564564564564564564564564564564564564564564564564564564564564564564564564564564564564564564564564564564564564564564564564564564564564564564564564564564564564564564564564564564564564564564564564564564564564564564564564564564564564564564564564564564564563333333456456456456456456456456456456456456456456456456456456456456456456456456456456456456456456456456456456456456456456332102102102102102102102102102102102102102102102102102102102102102102102102102102102102102102102103210210210210210210210210210210210210210210210210210210210210210'

# translating
x_long_t = translate_observations_to_indices(x_long)
z_long_t = translate_path_to_indices(z_long)

# testing count_transitions_and_emissions(K, D, x, z)
K, D = count_transitions_and_emissions(Kdim=7, Ddim=4, x=x_long_t, z=z_long_t)

# testing training by counting(Kdim, Ddim, x, z)
hmm_7_state_tbc = training_by_counting(Kdim=7, Ddim=4, x=x_long_t, z=z_long_t)
#print("hmm_7_state_tbc.init_probs =\n", hmm_7_state_tbc.init_probs)
#print("hmm_7_state_tbc.trans_probs =\n", hmm_7_state_tbc.trans_probs)
#print("hmm_7_state_tbc.emission_probs =\n", hmm_7_state_tbc.emission_probs)

# testing with viterbi (with log)
w = compute_w_log(hmm_7_state, x_long_t)
z_vit = backtrack_log(hmm_7_state, x_long_t, w)

w_tbc = compute_w_log(hmm_7_state_tbc, x_long_t)
z_vit_tbc = backtrack_log(hmm_7_state_tbc, x_long_t, w_tbc)

print("w == w_tbc overlap :\n", np.sum((w - w_tbc) < 10e-10) / np.size(w))
print("z_vit == z_vit_t overlap :\n", np.sum(z_vit[:] == z_vit_tbc[:])/len(z_vit) )

# def viterbi_training(init_model=None, x, z, updates=10, eps=10e-5, modelDims=[None, None]):

print("hmm_7_state_tbc.init_probs =\n", hmm_7_state_tbc.init_probs)
print("hmm_7_state_tbc.emission_probs =\n", hmm_7_state_tbc.emission_probs)

hmm_given = viterbi_training(x = x_long_t
                          ,z = z_long_t
                          ,init_model = hmm_7_state)
print("hmm_given.init_probs =\n", hmm_given.init_probs)
#print("hmm_given.trans_probs =\n", hmm_given.trans_probs)
print("hmm_given.emission_probs =\n", hmm_given.emission_probs)


hmm_random = viterbi_training(x = x_long_t
                             ,z = z_long_t
                             ,init_model = hmm_7_state_random)
print("hmm_random.init_probs =\n", hmm_random.init_probs)
#print("hmm_random.trans_probs =\n", hmm_random.trans_probs)
print("hmm_random.emission_probs =\n", hmm_random.emission_probs)


































