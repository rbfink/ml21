import math
import numpy as np
import time
# --- CODE PART 2 USING HMM FOR GENE FINDING -----------------------------------

def read_fasta_file(filename):
    """
    Reads the given FASTA file f and returns a dictionary of sequences.

    Lines starting with ';' in the FASTA file are ignored.
    """
    sequences_lines = {}
    current_sequence_lines = None
    with open(filename) as fp:
        for line in fp:
            line = line.strip()
            if line.startswith(';') or not line:
                continue
            if line.startswith('>'):
                sequence_name = line.lstrip('>')
                current_sequence_lines = []
                sequences_lines[sequence_name] = current_sequence_lines
            else:
                if current_sequence_lines is not None:
                    current_sequence_lines.append(line)
    sequences = {}
    for name, lines in sequences_lines.items():
        sequences[name] = ''.join(lines)
    return sequences

class hmm:
    def __init__(self, init_probs, trans_probs, emission_probs):
        self.init_probs = init_probs
        self.trans_probs = trans_probs
        self.emission_probs = emission_probs

def translate_indices_to_path(indices):
    mapping = ['C', 'C', 'C', 'N', 'R', 'R', 'R']
    return ''.join([mapping[i] for i in indices])

def translate_observations_to_indices(obs):
    mapping = {'a': 0, 'c': 1, 'g': 2, 't': 3}
    return [mapping[symbol.lower()] for symbol in obs]

def translate_indices_to_observations(indices):
    mapping = ['a', 'c', 'g', 't']
    return ''.join(mapping[idx] for idx in indices)

def make_table(m, n):
    """Make a table with `m` rows and `n` columns filled with zeros."""
    return np.zeros([m, n])

def log(x):
    if np.size(x) > 1:
        logx = np.zeros(len(x))
        for i in range(len(x)):
            if x[i] == 0:
                logx[i] = float('-inf')
            else:
                logx[i] = math.log(x[i])
        return logx
    elif x == 0:
        return float('-inf')
    else:
        return math.log(x)

def compute_w_log(model, x):
    K = len(model.init_probs)
    N = len(x)
    w = make_table(K, N)
    w[:,:] = float('-inf')
    # Base case: fill out w[i][0] for i = 0..k-1
    for i in range(0, K): #range(K-1)
        w[i, 0] = log(model.init_probs[i]) + log(model.emission_probs[i, x[0]])
    # Inductive case: fill out w[i][j] for i = 0..k, j = 0..n-1
    for n in range(1, N):
        for k in range(K):
            for j in range(K):
                w[k, n] = np.max([w[k, n], log(model.emission_probs[k, x[n]])
                                           + w[j, n-1]
                                           + log(model.trans_probs[j, k])])
    return w

def opt_path_prob_log(w):
    return np.max(w[:, -1])

def backtrack_log(model, x, w):
    K = len(model.init_probs)
    N = np.shape(w)[1]
    z = np.zeros(N, dtype=int)
    z[N-1] = np.argmax(w[:, N-1])
    # taking argmax relative to columns (k)
    for n in range(N-2, -1, -1):
        z[n] = np.argmax(log(model.emission_probs[z[n+1], x[n+1]]) + 
                         w[:, n] + 
                         log(model.trans_probs[:, z[n+1]]))
    #print("np.sum(z == 0) = ", np.sum(z == 0))
    #print("np.sum(z == 1) = ", np.sum(z == 1))
    #print("np.sum(z == 2) = ", np.sum(z == 2))
    #print("np.sum(z == 3) = ", np.sum(z == 3))
    #print("np.sum(z == 4) = ", np.sum(z == 4))
    #print("np.sum(z == 5) = ", np.sum(z == 5))
    #print("np.sum(z == 6) = ", np.sum(z == 6))
    
    return z

def count_transitions_and_emissions(Kdim, Ddim, x, z):
    """
    Returns a KxK matrix and a KxD matrix containing counts cf. above
    """
    K = make_table(Kdim, Kdim)
    D = make_table(Kdim, Ddim)
    N = len(z)
    for n in range(0, N):
        if n < N-1:
            # counting state transitions
            i = z[n]
            j = z[n + 1]
            K[i, j] += 1
        # counting emissions/outputs
        k = x[n]
        D[i, k] += 1
    return K, D

def training_by_counting(Kdim, Ddim, x, z):
    # counting init_params (always deterministic in this HMM model)
    Pi = np.zeros(Kdim)
    Pi[z[0]] = 1
    # counting trans_probs
    K, D = count_transitions_and_emissions(Kdim, Ddim, x, z)
    # adding seudo counts with mask
    K += (K == 0)
    D += (D == 0)
    Ksum = np.sum(K, axis=1)
    Dsum = np.sum(D, axis=1)
    for row in range(Kdim):
        K[row, :] = K[row, :] / Ksum[row]
        D[row, :] = D[row, :] / Dsum[row]
    return hmm(Pi, K, D)

def viterbi_update_model(model, x):
    """
    return a new model that corresponds to one round of Viterbi training, 
    i.e. a model where the parameters reflect training by counting on x 
    and z_vit, where z_vit is the Viterbi decoding of x under the given 
    model.
    """
    # initial model parameters are given in "model", now calculating "w"
    w = compute_w_log(model, x)
    # doing backtracking using new "w"
    z_new = backtrack_log(model, x, w)
    # now doing TbC to update model parameters
    hmm_update = training_by_counting(Kdim = np.shape(model.trans_probs)[0]
                                     ,Ddim = np.shape(model.emission_probs)[1]
                                     ,x = x
                                     ,z = z_new)
    return hmm_update

def viterbi_training(x, z, init_model=None, updates=10, eps=10e-5, modelDims=np.array([None, None])):
    """
    Parameters :
        init_model : initial model parameters of the hmm class
        x : outputs of the hmm
        z : known latent states in the hmm
        updates : max number of viterbi updates before returning
        eps : sensitivity of early stopping
    """
    
    if init_model == None and np.sum(modelDims == None) == 0:
        init_model = training_by_counting(modelDims[0], modelDims[1], x, z)
    elif init_model != None and np.sum(modelDims == None) == 0:
        print("\n Error in viterbi_training input, either in init_model or modelDims")
        return None
    elif init_model == None and np.sum(modelDims == None) != 0:
        print("\n Error in viterbi_training input, either in init_model or modelDims")
        return None

    model = init_model
    update = 0
    while update < updates:
        print("viterbi training update {0}/{1}".format(update+1, updates))
        # run update
        model = viterbi_update_model(model, x)
        update += 1
    return model

def translate_path_to_indices(path):
    indices = [None for i in range(len(path))]
    Cs = 2
    Rs = 4
    for i in range(len(path)):
        if path[i] == "N":
            indices[i] = 3
            Cs = 2
            Rs = 4
        elif path[i] == "C":
            Rs = 4
            if Cs >= 0:
                indices[i] = Cs
                Cs -= 1
            else:
                indices[i] = 2
                Cs = 1
        elif path[i] == "R":
            Cs = 2
            if Rs <= 6:
                indices[i] = Rs
                Rs += 1
            else:
                indices[i] = 4
                Rs = 5
    return indices

def final_annotation(model, x):
    w = compute_w_log(model, x)
    z = backtrack_log(model, x, w)
    return z

# --- CODE ---------------------------------------------------------------------

init_probs_7_state = np.array([0.00, 0.00, 0.00, 1.00, 0.00, 0.00, 0.00])

trans_probs_7_state = np.array([
    [0.00, 0.00, 0.90, 0.10, 0.00, 0.00, 0.00],
    [1.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00],
    [0.00, 1.00, 0.00, 0.00, 0.00, 0.00, 0.00],
    [0.00, 0.00, 0.05, 0.90, 0.05, 0.00, 0.00],
    [0.00, 0.00, 0.00, 0.00, 0.00, 1.00, 0.00],
    [0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 1.00],
    [0.00, 0.00, 0.00, 0.10, 0.90, 0.00, 0.00],
])

emission_probs_7_state = np.array([
    #   A     C     G     T
    [0.30, 0.25, 0.25, 0.20],
    [0.20, 0.35, 0.15, 0.30],
    [0.40, 0.15, 0.20, 0.25],
    [0.25, 0.25, 0.25, 0.25],
    [0.20, 0.40, 0.30, 0.10],
    [0.30, 0.20, 0.30, 0.20],
    [0.15, 0.30, 0.20, 0.35],
])

hmm_7_state = hmm(init_probs_7_state, trans_probs_7_state, emission_probs_7_state)

# calculate annotaion using hmm_7_state and 
g1_dict = read_fasta_file("/home/rbf/repos/ml21/handins/handin3/genomes/genome1.fa")
g1_true = read_fasta_file("/home/rbf/repos/ml21/handins/handin3/genomes/true-ann1.fa")
g2_dict = read_fasta_file("/home/rbf/repos/ml21/handins/handin3/genomes/genome2.fa")
g2_true = read_fasta_file("/home/rbf/repos/ml21/handins/handin3/genomes/true-ann2.fa")

g1_x = translate_observations_to_indices(g1_dict["genome1"][0:30000])
g1_z = translate_path_to_indices(g1_true["true-ann1"][0:30000])
g2_x = translate_observations_to_indices(g2_dict["genome2"][0:30000])
g2_z = translate_path_to_indices(g2_true["true-ann2"][0:30000])


g1_model = viterbi_training(g1_x, g1_z, init_model=hmm_7_state, updates=5)
#g2_model = viterbi_training(g2_x, g2_z, init_model=hmm_7_state, updates=5)

print(g1_model.emission_probs)
#print(g1_model.trans_probs)

#print(g2_model.emission_probs)
#print(g2_model.trans_probs)

# new path given optimized model
g1_path = final_annotation(g1_model, g1_x)

print("g1 overlap : ", np.sum(g1_z[:] == g1_path[:]) / len(g1_z))

























