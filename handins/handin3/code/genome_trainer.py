import math
import numpy as np
import time as time

def read_fasta_file(filename):
    """
    Reads the given FASTA file f and returns a dictionary of sequences.

    Lines starting with ';' in the FASTA file are ignored.
    """
    sequences_lines = {}
    current_sequence_lines = None
    with open(filename) as fp:
        for line in fp:
            line = line.strip()
            if line.startswith(';') or not line:
                continue
            if line.startswith('>'):
                sequence_name = line.lstrip('>')
                current_sequence_lines = []
                sequences_lines[sequence_name] = current_sequence_lines
            else:
                if current_sequence_lines is not None:
                    current_sequence_lines.append(line)
    sequences = {}
    for name, lines in sequences_lines.items():
        sequences[name] = ''.join(lines)
    return sequences

class hmm:
    def __init__(self, init_probs, trans_probs, emission_probs):
        self.init_probs = init_probs
        self.trans_probs = trans_probs
        self.emission_probs = emission_probs

def translate_indices_to_path(indices):
    mapping = ['C', 'C', 'C', 'N', 'R', 'R', 'R']
    return ''.join([mapping[i] for i in indices])

def translate_observations_to_indices(obs):
    mapping = {'a': 0, 'c': 1, 'g': 2, 't': 3}
    return [mapping[symbol.lower()] for symbol in obs]

def translate_indices_to_observations(indices):
    mapping = ['a', 'c', 'g', 't']
    return ''.join(mapping[idx] for idx in indices)

def make_table(m, n):
    """Make a table with `m` rows and `n` columns filled with zeros."""
    return np.zeros([m, n])

def log(x):
    if np.size(x) > 1:
        logx = np.zeros(len(x))
        for i in range(len(x)):
            if x[i] == 0:
                logx[i] = float('-inf')
            else:
                logx[i] = math.log(x[i])
        return logx
    elif x == 0:
        return float('-inf')
    else:
        return math.log(x)

def compute_w_log(model, x):
    K = len(model.init_probs)
    N = len(x)
    w = make_table(K, N)
    w[:,:] = float('-inf')
    # Base case: fill out w[i][0] for i = 0..k-1
    for i in range(0, K): #range(K-1)
        w[i, 0] = log(model.init_probs[i]) + log(model.emission_probs[i, x[0]])
    # Inductive case: fill out w[i][j] for i = 0..k, j = 0..n-1
    for n in range(1, N):
        #print("compute_w_log n/N = {0}/{1}]".format(n, N))
        for k in range(K):
            for j in range(K):
                w[k, n] = np.max([w[k, n], log(model.emission_probs[k, x[n]])
                                           + w[j, n-1]
                                           + log(model.trans_probs[j, k])])
    return w

def opt_path_prob_log(w):
    return np.max(w[:, -1])

def backtrack_log(model, x, w):
    K = len(model.init_probs)
    N = len(x)
    z = np.zeros(N, dtype=int)
    z[N-1] = np.argmax(w[:, N-1])
    # taking argmax relative to columns (k)
    for n in range(N-2, -1, -1):
        z[n] = np.argmax(log(model.emission_probs[z[n+1], x[n+1]]) + 
                         w[:, n] + 
                         log(model.trans_probs[:, z[n+1]]))
    return z

def count_transitions_and_emissions19(Kdim, Ddim, x, z):
    K = np.zeros([Kdim, Kdim], dtype=float)
    D = np.zeros([Kdim, Ddim], dtype=float)
    N = len(z)
    n = 1
    D[0, x[0]] += 1
    while n < N - 21:
#        print("{0}/{1}".format(n,N))
        # counting transitions

        z1 = z[n-1]
        z2 = z[n]
        zsum = int(z1 + z2)
        
        if zsum == 0: # N -> N
#            print("N -> N")
            K[0, 0] += 1
            # counting emissions
            D[0, x[n]] += 1
            n += 1

        elif zsum == 1: # N -> C
#            print("N -> C")
            K[0, 1] += 1
            # counting transisions for start codon (CCC)
            for i in range(1, 4):
                D[i, x[n+i-1]] += 1
            # counting transisions for initial coding triplet
            for i in range(4, 7):
                D[i, x[n+i-1]] += 1
            n += 6

        elif zsum == 2: # N -> R or C -> C
            if z1 == 0: # N -> R 
#                print("N -> R")
                K[0, 10] += 1
                # counting transisions for start codon (RRR)
                for i in range(10, 13):
                    D[i, x[n+i-1]] += 1
                # couting transisions for initial coding triplet
                for i in range(13, 16):
                    D[i, x[n+i-1]] += 1
                n += 6
            else: # C -> C 
#                print("C -> C")
                if z[n+3] != 0: # C coding
                    K[6, 4] += 1
                    # counting transision for coding triplet
                    for i in range(4, 7):
                        D[i, x[n+i-1]] += 1
                    n += 3
                else: # C stop codon
                    K[6, 7] += 1
                    # count transisions for stop codon
                    for i in range(7, 10):
                        D[i, x[n+i-1]] += 1
                    n += 3

        elif zsum == 4: # R -> R
#            print("R -> R")
            if z[n+3] != 0: # R coding
                K[15, 13] += 1
                # coutning transisions for R coding triplet
                for i in range(13, 16):
                    D[i, x[n+i-1]] += 1
                n += 3
            else: # R stop codon
                K[15, 16] += 1
                # coutning transisions for R stop codon
                for i in range(16, 19):
                    D[i, x[n+i-1]] += 1
                n += 3

        elif zsum == 3: # C -> R or R -> C
            if z1 == 1: # C -> R
#                print("C -> R")
                K[6, 7] += 1
                K[9, 10] += 1
                K[12, 13] += 1
                # counting C stop codon
                for i in range(7, 10):
                    D[i, x[n+i-1]] += 1
                # counting R start codon
                for i in range(10, 13):
                    D[i, x[n+i-1]] += 1
                # counting R initial code
                for i in range(13, 16):
                    D[i, x[n+i-1]] += 1
                n += 9
            else: # R -> C
#                print("R -> C")
                K[15, 16] += 1
                K[18, 1] += 1
                K[3, 4] += 1
                # counting R stop codon
                for i in range(16, 19):
                    D[i, x[n+i-1]] += 1
                # counting C start codon
                for i in range(1, 4):
                    D[i, x[n+i-1]] += 1
                # counting C initial code
                for i in range(4, 7):
                    D[i, x[n+i-1]] += 1
                n += 9

    # hardcoding transitions
    # coding start codon
    K[1, :] = np.zeros(Kdim)
    K[1, 2] = 1.0
    K[2, :] = np.zeros(Kdim)
    K[2, 3] = 1.0
    # coding coding
    K[3, :] = np.zeros(Kdim)
    K[3, 4] = 1.0
    K[4, :] = np.zeros(Kdim)
    K[4, 5] = 1.0
    K[5, :] = np.zeros(Kdim)
    K[5, 6] = 1.0
    # coding stop codon
    K[7, :] = np.zeros(Kdim)
    K[7, 8] = 1.0
    K[8, :] = np.zeros(Kdim)
    K[8, 9] = 1.0
    # reverse coding start codon
    K[10, :] = np.zeros(Kdim)
    K[10, 11] = 1.0
    K[11, :] = np.zeros(Kdim)
    K[11, 12] = 1.0
    K[12, :] = np.zeros(Kdim)
    K[12, 13] = 1.0
    # reverse coding coding
    K[13, :] = np.zeros(Kdim)
    K[13, 14] = 1.0
    K[14, :] = np.zeros(Kdim)
    K[14, 15] = 1.0
    # reverse coding stop codon 
    K[16, :] = np.zeros(Kdim)
    K[16, 17] = 1.0
    K[17, :] = np.zeros(Kdim)
    K[17, 18] = 1.0

    return K, D
#        elif (z[n-1] == 0 and z[n] == 1): # N -> C (start codon)
#            print("N -> C (start codon)")
#            K[0, 1] += 1
#            # counting emissions
#            for i in range(1, 4):
#                D[i, x[n+i-1]] += 1
#            n += 3
#
#        elif (z[n-1] == 1 and z[n+3] == 0): # C -> N (stop codon)
#            print("C -> N (stop codon)")
#            K[6, 7] += 1
#            # counting emissions
#            for i in range(7, 10):
#                D[i, x[n+i-1]] += 1
#            n += 3
#
#        elif (z[n-1] == 1 and z[n+3] == 2): # C -> R (C stop and R start)
#            print("C -> R (C stop and R start)")
#            K[6, 7]  += 1 
#            K[9, 10] += 1
#            # counting emissions
#            for i in range(7, 10):
#                D[i, x[n+i-1]] += 1
#            for i in range(10, 13):
#                D[i, x[n+i-1]] += 1
#            n += 6
#
#        elif (z[n-1] == 1 and z[n] == 1): # C -> C (coding)
#            print("C -> C (coding)")
#            K[6, 4] += 1
#            # counting emissions
#            for i in range(4,7):
#                D[i, x[n+i-1]] += 1
#            n += 3
#
#        elif (z[n-1] == 0 and z[n] == 2): # N -> R (start codon)
#            print("N -> R (start codon)")
#            K[0, 10] += 1
#            # counting emissions
#            for i in range(10, 13):
#                D[i, x[n+i-1]] += 1
#            n += 3
#
#        elif (z[n-1] == 2 and z[n+3] == 0): # R -> N (stop codon)
#            print("R -> N (stop codon)")
#            K[15, 16] += 1
#            # counting emissions
#            for i in range(16, 19):
#                D[i, x[n+i-1]] += 1
#            n += 3
#
#        elif (z[n-1] == 2 and z[n+3] == 1): # R -> C
#            print("R -> C")
#            K[15, 16] += 1
#            K[18, 1] += 1
#            # counting emissions
#            for i in range(16, 19):
#                D[i, x[n+i-1]] += 1
#            for i in range(1, 4):
#                D[i, x[n+i-1]] += 1
#            n += 6
#
#        elif (z[n-1] == 2 and z[n] == 2): # R -> R (coding)
#            print("R -> R (coding)")
#            K[15, 13] += 1
#            # counting emissions
#            for i in range(13, 16):
#                D[i, x[n+i-1]] += 1
#            n += 3
#
#    return K, D

def training_by_counting19(Kdim, Ddim, x, z):
    # counting init_params (always deterministic in this HMM model)
    Pi = np.zeros(Kdim)
    Pi[0] = 1.0
    # counting trans_probs
    print("counting")
    K, D = count_transitions_and_emissions19(Kdim, Ddim, x, z)
    print("counting finished")
    # adding seudo counts with mask
    K += (K == 0.0)
    D += (D == 0.0)
    Ksum = np.sum(K, axis=1)
    Dsum = np.sum(D, axis=1)
    for row in range(Kdim):
        K[row, :] = K[row, :] / Ksum[row]
        D[row, :] = D[row, :] / Dsum[row]
    return hmm(Pi, K, D)

def viterbi_update_model(model, x):
    """
    return a new model that corresponds to one round of Viterbi training, 
    i.e. a model where the parameters reflect training by counting on x 
    and z_vit, where z_vit is the Viterbi decoding of x under the given 
    model.
    """
    # initial model parameters are given in "model", now calculating "w"
    w = compute_w_log(model, x)
    # doing backtracking using new "w"
    z_new = backtrack_log(model, x, w)
    # now doing TbC to update model parameters
    hmm_update = training_by_counting19(Kdim = np.shape(model.trans_probs)[0]
                                       ,Ddim = np.shape(model.emission_probs)[1]
                                       ,x = x
                                       ,z = z_new)
    return hmm_update

def viterbi_training(x, z, init_model=None, updates=10, eps=10e-5, modelDims=np.array([None, None])):
    """
    Parameters :
        init_model : initial model parameters of the hmm class
        x : outputs of the hmm
        z : known latent states in the hmm
        updates : max number of viterbi updates before returning
        eps : sensitivity of early stopping
    """
    
    if init_model == None and np.sum(modelDims == None) == 0:
        init_model = training_by_counting(modelDims[0], modelDims[1], x, z)
    elif init_model != None and np.sum(modelDims == None) == 0:
        print("\n Error in viterbi_training input, either in init_model or modelDims")
        return None
    elif init_model == None and np.sum(modelDims == None) != 0:
        print("\n Error in viterbi_training input, either in init_model or modelDims")
        return None

    model = init_model
    update = 0
    while update < updates:
        print("viterbi training update {0}/{1}".format(update+1, updates))
        # run update
        model = viterbi_update_model(model, x)
        update += 1
    return model

def translate_path_to_indices(path):
    obs = np.array(list(path))
    mapping = {'N': 0, 'C': 1, 'R': 2}
    return [mapping[symbol] for symbol in obs]

def translate_states_to_path(z):
    mapping = ['N','C','C','C','C','C','C','C','C','C','R','R','R','R','R','R','R','R','R']
    long_string = ''.join([mapping[i] for i in z])
    return np.array(list(long_string))

def final_annotation(model, x):
    print("final annotaition : calculating w")
    w = compute_w_log(model, x)
    print("final annotaition : calculating z")
    z = backtrack_log(model, x, w)
    print("z before translating... :", z)
    print("final annotaition : translating z")
    z = translate_states_to_path(z)
    return z

def compute_accuracy(true_ann, pred_ann):
    if len(true_ann) != len(pred_ann):
        return 0.0
    return sum(1 if true_ann[i] == pred_ann[i] else 0 
               for i in range(len(true_ann))) / len(true_ann)

# --- CODE ---------------------------------------------------------------------
# harcoding model
Kdim = 19
Ddim = 4

# testing on genome 1
g1 = read_fasta_file("/home/rbf/repos/ml21/handins/handin3/genomes/genome1.fa")["genome1"]#[60000:100000]#[60000:160000]
true_ann1 = read_fasta_file("/home/rbf/repos/ml21/handins/handin3/genomes/true-ann1.fa")["true-ann1"]#[60000:100000]#[60000:160000]

print(true_ann1[0])

g1_x = translate_observations_to_indices(g1)
g1_z = translate_path_to_indices(true_ann1)

g1_model = training_by_counting19(Kdim, Ddim, g1_x, g1_z)


print("calculating final annotation")
t = time.time()
g1_path = np.array(final_annotation(g1_model, g1_x))
print("final annotation finished, time to run : {0}".format(time.time()-t))


print("g1_path =", g1_path)
true_ann1_compare = np.array(list(true_ann1))

AC_genome1 = compute_accuracy(true_ann1, "".join(g1_path))

with open("true_ann.fa", "w") as f:
    f.write(true_ann1)
f.close()
with open("g1_path.fa", "w") as f:
    f.write("".join(g1_path))
f.close()




print("g1_model.init_probs =\n", g1_model.init_probs)
print("g1_model.trans_probs =\n", g1_model.trans_probs)
print("g1_model.emission =\n", g1_model.emission_probs)
print("accuracy/overlap =", AC_genome1)





















