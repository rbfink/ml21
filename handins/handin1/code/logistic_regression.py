import numpy as np
import time as time
from h1_util import numerical_grad_check

def logistic(z):
    """ 
    Helper function
    Computes the logistic function 1/(1+e^{-x}) to each entry in input vector z.
    
    np.exp may come in handy
    Args:
        z: numpy array shape (d,) 
    Returns:
       logi: numpy array shape (d,) each entry transformed by the logistic function 
    """
    logi = np.zeros(z.shape)
    ### YOUR CODE HERE 1-5 lines

    if logi.shape != ():
        logi = 1/(1 + np.exp(-z[:]))
    else:
        logi = 1/(1 + np.exp(-z))

    ### END CODE
    assert logi.shape == z.shape
    return logi


class LogisticRegressionClassifier():

    def __init__(self):
        self.w = None

    def cost_grad(self, X, y, w):
        """
        Compute the average negative log likelihood and gradient under the logistic regression model 
        using data X, targets y, weight vector w 
        
        np.log, np.sum, np.choose, np.dot may be useful here
        Args:
           X: np.array shape (n,d) float - Features 
           y: np.array shape (n,)  int - Labels 
           w: np.array shape (d,)  float - Initial parameter vector

        Returns:
           cost: scalar: the average negative log likelihood for logistic regression with data X, y 
           grad: np.arrray shape(d, ) gradient of the average negative log likelihood at w 
        """
        cost = 0
        grad = np.zeros(w.shape)
        ### YOUR CODE HERE 5 - 15 lines

        n = y.shape[0]
        # calculating the pointwise costfunction and gradient
        for i in range(n):
            cost += np.log(1 + np.exp( -y[i]*np.dot(w, X[i, :]) ))
            grad += -y[i]*X[i, :]*logistic( -y[i]*np.dot(w, X[i, :]) )                        
        cost = cost/n
        grad = grad/n

        ### END CODE
        assert grad.shape == w.shape
        return cost, grad


    def fit(self, X, y, w=None, lr=0.1, batch_size=16, epochs=10):
        """
        Run mini-batch stochastic Gradient Descent for logistic regression 
        use batch_size data points to compute gradient in each step.
    
        The function np.random.permutation may prove useful for shuffling the data before each epoch
        It is wise to print the performance of your algorithm at least after every epoch to see if progress is being made.
        Remeber the stochastic nature of the algorithm may give fluctuations in the cost as iterations increase.

        Args:
           X: np.array shape (n,d) dtype float32 - Features 
           y: np.array shape (n,) dtype int32 - Labels 
           w: np.array shape (d,) dtype float32 - Initial parameter vector
           lr: scalar - learning rate for gradient descent
           batch_size: number of elements to use in minibatch

           epochs: Number of scans through the data

        sets: 
           w: numpy array shape (d,) learned weight vector w
           history: list/np.array len epochs - value of cost function after every epoch. You know for plotting
        """
        if w is None: w = np.zeros(X.shape[1])
        history = []        
        ### YOUR CODE HERE 14 - 20 lines
        t1 = time.time()
        n = X.shape[0]
        # inizializing fitting vector w
        w = np.random.uniform(low=0, high=1, size=w.shape)
        # preallocating
        epoch = 0
        X_batch = np.zeros((batch_size, X.shape[1]))
        y_batch = np.zeros(batch_size)
        # stochastic gradient descent
        while epoch < epochs:
            # randomize the feature vectors indices for this epoch
            X_perm_index = np.random.permutation(n)
            for i in range( np.floor(n/batch_size).astype(int) ):
                # create current batch
                X_batch = X[X_perm_index[i*batch_size:(i+1)*batch_size], :]
                y_batch = y[X_perm_index[i*batch_size:(i+1)*batch_size]]
                # calculate gradient
                cost, grad = self.cost_grad(X_batch, y_batch, w)
                w = w - lr*grad
            history.append(cost)
            epoch += 1
            # lowering learning rate five times
            if epoch%(np.floor(epochs/5)) == 0:
                lr *= 0.9
        t2 = time.time()
        print(" - Dimensionality [n, d] = [{0}, {1}]", (X.shape[0], X.shape[1]))
        print(" - Mini batch gradient descent running time =", t2-t1)
        ### END CODE
        self.w = w
        self.history = history


    def predict(self, X):
        """ Classify each data element in X

        Args:
            X: np.array shape (n,d) dtype float - Features 
        
        Returns: 
           p: numpy array shape (n, ) dtype int32, class predictions on X (-1, 1)

        """
        out = np.ones(X.shape[0])
        ### YOUR CODE HERE 1 - 4 lines

        for i in range(len(out)):
            if logistic(np.dot(self.w, X[i, :])) < 0.5:
                out[i] = out[i] * -1

        ### END CODE
        return out
    
    def score(self, X, y):
        """ Compute model accuracy  on Data X with labels y

        Args:
            X: np.array shape (n,d) dtype float - Features 
            y: np.array shape (n,) dtype int - Labels 

        Returns: 
           s: float, number of correct prediction divivded by n.

        """
        s = 0
        ### YOUR CODE HERE 1 - 4 lines

        pred = self.predict(X)
        n = np.shape(pred)
        s = np.sum(pred == y)/n

        ### END CODE
        return s
        

    
def test_logistic():
    print('*'*5, 'Testing logistic function')
    a = np.array([0, 1, 2, 3])
    lg = logistic(a)
    target = np.array([ 0.5, 0.73105858, 0.88079708, 0.95257413])
    assert np.allclose(lg, target), 'Logistic Mismatch Expected {0} - Got {1}'.format(target, lg)
    print('Test Success!')

    
def test_cost():
    print('*'*5, 'Testing Cost Function')
    X = np.array([[1.0, 0.0], [1.0, 1.0], [3, 2]])
    y = np.array([-1, -1, 1], dtype='int64')
    w = np.array([0.0, 0.0])
    print('shapes', X.shape, w.shape, y.shape)
    lr = LogisticRegressionClassifier()
    cost,_ = lr.cost_grad(X, y, w)
    target = -np.log(0.5)
    assert np.allclose(cost, target), 'Cost Function Error:  Expected {0} - Got {1}'.format(target, cost)
    print('Test Success')

    
def test_grad():
    print('*'*5, 'Testing  Gradient')
    X = np.array([[1.0, 0.0], [1.0, 1.0], [2.0, 3.0]])    
    w = np.array([0.0, 0.0])
    y = np.array([-1, -1, 1]).astype('int64')
    print('shapes', X.shape, w.shape, y.shape)
    lr = LogisticRegressionClassifier()
    f = lambda z: lr.cost_grad(X, y, w=z)
    numerical_grad_check(f, w)
    print('Test Success')


    
if __name__ == '__main__':
    test_logistic()
    test_cost()
    test_grad()
    
    
