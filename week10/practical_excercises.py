import math  # Just ignore this :-)
import numpy as np

init_probs_7_state = np.array([0.00, 0.00, 0.00, 1.00, 0.00, 0.00, 0.00])

trans_probs_7_state = np.array([
    [0.00, 0.00, 0.90, 0.10, 0.00, 0.00, 0.00],
    [1.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00],
    [0.00, 1.00, 0.00, 0.00, 0.00, 0.00, 0.00],
    [0.00, 0.00, 0.05, 0.90, 0.05, 0.00, 0.00],
    [0.00, 0.00, 0.00, 0.00, 0.00, 1.00, 0.00],
    [0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 1.00],
    [0.00, 0.00, 0.00, 0.10, 0.90, 0.00, 0.00],
])

emission_probs_7_state = np.array([
    #   A     C     G     T
    [0.30, 0.25, 0.25, 0.20],
    [0.20, 0.35, 0.15, 0.30],
    [0.40, 0.15, 0.20, 0.25],
    [0.25, 0.25, 0.25, 0.25],
    [0.20, 0.40, 0.30, 0.10],
    [0.30, 0.20, 0.30, 0.20],
    [0.15, 0.30, 0.20, 0.35],
])

obs_example = 'GTTTCCCAGTGTATATCGAGGGATACTACGTGCATAGTAACATCGGCCAA'

def translate_observations_to_indices(obs):
    mapping = {'a': 0, 'c': 1, 'g': 2, 't': 3}
    return [mapping[symbol.lower()] for symbol in obs]

obs_example_trans = translate_observations_to_indices(obs_example)

obs_example_trans

def translate_indices_to_observations(indices):
    mapping = ['a', 'c', 'g', 't']
    return ''.join(mapping[idx] for idx in indices)

translate_indices_to_observations(translate_observations_to_indices(obs_example))

def translate_path_to_indices(path):
    return list(map(lambda x: int(x), path))

def translate_indices_to_path(indices):
    return ''.join([str(i) for i in indices])

path_example = '33333333333321021021021021021021021021021021021021'

translate_path_to_indices(path_example)

class hmm:
    def __init__(self, init_probs, trans_probs, emission_probs):
        self.init_probs = init_probs
        self.trans_probs = trans_probs
        self.emission_probs = emission_probs

# Collect the matrices in a class.
hmm_7_state = hmm(init_probs_7_state, trans_probs_7_state, emission_probs_7_state)

# We can now reach the different matrices by their names. E.g.:
hmm_7_state.trans_probs

init_probs_3_state = np.array([0.10, 0.80, 0.10])

trans_probs_3_state = np.array([
    [0.90, 0.10, 0.00],
    [0.05, 0.90, 0.05],
    [0.00, 0.10, 0.90],
])

emission_probs_3_state = np.array([
    #   A     C     G     T
    [0.40, 0.15, 0.20, 0.25],
    [0.25, 0.25, 0.25, 0.25],
    [0.20, 0.40, 0.30, 0.10],
])

hmm_3_state = hmm(init_probs_3_state, trans_probs_3_state, emission_probs_3_state)

# --- VALIDATION FUNCTION ---
def validate_hmm(model):

    init = model.init_probs
    trans = model.trans_probs
    emit = model.emission_probs
    states = np.shape(trans)[0]
    test = np.ones(4)
    
    # testing summation of probabilities
    test[0] *= np.sum(init) == 1.0
    test[1] *= np.sum(np.sum(trans, axis=1)) == states
    test[2] *= np.sum(np.sum(emit, axis=1)) == states

    # testing that all numbers are within [0, 1]
    test[3] *= np.sum((init>=0)*(init<=1.0)) == init.size
    test[3] *= np.sum((trans>=0)*(trans<=1.0)) == trans.size
    test[3] *= np.sum((emit>=0)*(emit<=1.0)) == emit.size

    if np.sum(test) == test.size:
        return True
    else:
        return False

validate_hmm(hmm_7_state)

# --- JOINT PROBABILITY WITHOUT LOG ---
def joint_prob(model, x, z):
    p = model.init_probs[z[0]] * model.emission_probs[z[0], x[0]]
    for i in range(1, len(x)):
        p *= model.trans_probs[z[i-1], z[i]] * model.emission_probs[z[i], x[i]]
    return p

x_short = 'GTTTCCCAGTGTATATCGAGGGATACTACGTGCATAGTAACATCGGCCAA'
z_short = '33333333333321021021021021021021021021021021021021'

# Your code here ...
translated_obs  = translate_observations_to_indices(x_short)
translated_path = translate_path_to_indices(z_short)

prob_no_log = joint_prob(hmm_7_state, translated_obs, translated_path)

# --- JOINT PROBABILITY WITH LOG ---
def log(x):
    if np.size(x) > 1:
        logx = np.zeros(len(x))
        for i in range(len(x)):
            if x[i] == 0:
                logx[i] = float('-inf')
            else:
                logx[i] = math.log(x[i])
        return logx
    elif x == 0:
        return float('-inf')
    else:
        return math.log(x)

def joint_prob_log(model, x, z):
    logp = log(model.init_probs[z[0]]) + log(model.emission_probs[z[0], x[0]])
    for i in range(1, len(x)):
        logp += log(model.trans_probs[z[i-1], z[i]]) + log(model.emission_probs[z[i], x[i]])
    return logp

# testing joint probability methods
jprob_no_log = joint_prob(hmm_7_state, translated_obs, translated_path)
jprob_log = joint_prob_log(hmm_7_state, translated_obs, translated_path)
print("jprob_no_log =\n", jprob_no_log)
print("log(jprob_no_log) =\n", log(jprob_no_log))
print("jprob_log =\n", jprob_log)
print("Joint probability with and without log is equal :\n", jprob_no_log == jprob_log)


# now testing the breakpoint of the naive no-log joint probability
x_long = 'TGAGTATCACTTAGGTCTATGTCTAGTCGTCTTTCGTAATGTTTGGTCTTGTCACCAGTTATCCTATGGCGCTCCGAGTCTGGTTCTCGAAATAAGCATCCCCGCCCAAGTCATGCACCCGTTTGTGTTCTTCGCCGACTTGAGCGACTTAATGAGGATGCCACTCGTCACCATCTTGAACATGCCACCAACGAGGTTGCCGCCGTCCATTATAACTACAACCTAGACAATTTTCGCTTTAGGTCCATTCACTAGGCCGAAATCCGCTGGAGTAAGCACAAAGCTCGTATAGGCAAAACCGACTCCATGAGTCTGCCTCCCGACCATTCCCATCAAAATACGCTATCAATACTAAAAAAATGACGGTTCAGCCTCACCCGGATGCTCGAGACAGCACACGGACATGATAGCGAACGTGACCAGTGTAGTGGCCCAGGGGAACCGCCGCGCCATTTTGTTCATGGCCCCGCTGCCGAATATTTCGATCCCAGCTAGAGTAATGACCTGTAGCTTAAACCCACTTTTGGCCCAAACTAGAGCAACAATCGGAATGGCTGAAGTGAATGCCGGCATGCCCTCAGCTCTAAGCGCCTCGATCGCAGTAATGACCGTCTTAACATTAGCTCTCAACGCTATGCAGTGGCTTTGGTGTCGCTTACTACCAGTTCCGAACGTCTCGGGGGTCTTGATGCAGCGCACCACGATGCCAAGCCACGCTGAATCGGGCAGCCAGCAGGATCGTTACAGTCGAGCCCACGGCAATGCGAGCCGTCACGTTGCCGAATATGCACTGCGGGACTACGGACGCAGGGCCGCCAACCATCTGGTTGACGATAGCCAAACACGGTCCAGAGGTGCCCCATCTCGGTTATTTGGATCGTAATTTTTGTGAAGAACACTGCAAACGCAAGTGGCTTTCCAGACTTTACGACTATGTGCCATCATTTAAGGCTACGACCCGGCTTTTAAGACCCCCACCACTAAATAGAGGTACATCTGA'
z_long = '3333321021021021021021021021021021021021021021021021021021021021021021033333333334564564564564564564564564564564564564564564564564564564564564564564564564564564564564564564564564564564564564564564564564564564563210210210210210210210210210210210210210210210210210210210210210210210210210210210210210210210210210210210210210210210210210210210321021021021021021021021033334564564564564564564564564564564564564564564564564564564564564564564564564564564564564564564564564564564564564564564564564564564564564564564564564564564564564564564564564564564564564564564564564564564564564564564564564564564564564564564564564564564564564564564564564564564564564564564564564564564564564564564564564564564564564564564564564564564564563333333456456456456456456456456456456456456456456456456456456456456456456456456456456456456456456456456456456456456456456332102102102102102102102102102102102102102102102102102102102102102102102102102102102102102102102103210210210210210210210210210210210210210210210210210210210210210'

for i in range(100, len(x_long), 100):
    x = x_long[:i]
    z = z_long[:i]
    
    x_trans = translate_observations_to_indices(x)
    z_trans = translate_path_to_indices(z)
    
    # Make your experiment here...
    jpnl = joint_prob(hmm_7_state, x_trans, z_trans)
    jpl = joint_prob_log(hmm_7_state, x_trans, z_trans)
    print("i =", i)
    print("jpnl :", jpnl)
    print("jpl =", jpl)

# --- IMPLEMENTING VITERBI ALGORITHM WITHOUT LOG -------------------------------
def make_table(m, n):
    """Make a table with `m` rows and `n` columns filled with zeros."""
    return np.zeros([m, n])

def compute_w(model, x):
    K = len(model.init_probs)
    N = len(x)
    
    w = make_table(K, N)
    
    # Base case: fill out w[i][0] for i = 0..k-1
    for i in range(0,K-1):
        w[i, 0] = model.init_probs[i]*model.emission_probs[i, x[0]]
    # Inductive case: fill out w[i][j] for i = 0..k, j = 0..n-1
    for n in range(1, N):
        for k in range(K):
            for j in range(K):
                w[k, n] = np.max([w[k, n], model.emission_probs[k, x[n]]*w[j, n-1]*model.trans_probs[j, k]])
    return w

# translating x_long
x_long_trans = translate_observations_to_indices(x_long)
x_short_trans = translate_observations_to_indices(x_short) 

def opt_path_prob(w):
    return np.max(w[:, -1])

w_short_nl = compute_w(hmm_7_state, x_short_trans)
w_long_nl = compute_w(hmm_7_state, x_long_trans)

print("\n (no log) opt_path_prob(w_short_nl) =", opt_path_prob(w_short_nl))
print(" (no log) opt_path_prob(w_long_nl) =", opt_path_prob(w_long_nl))

# backtracking
def backtrack(model, x, w):
    K = len(model.init_probs)
    N = len(x)
    z = np.zeros(N, dtype=int)
    
    z[-1] = np.argmax(w[:, -1])
    # taking argmax relative to columns (k)
    for n in range(N-2, -1, -1):
        z[n] = np.argmax(model.emission_probs[z[n+1], x[n+1]]*w[:, n]*model.trans_probs[:, z[n+1]])
    return z

z_short_viterbi = backtrack(hmm_7_state, x_short_trans, w_short_nl)
z_long_viterbi = backtrack(hmm_7_state, x_long_trans, w_long_nl)

#print("z_short_viterbi =", z_short_viterbi)
#print("z_long_viterbi =", z_long_viterbi)

# --- IMPLEMENTING VITERBI WITH LOG --------------------------------------------
def compute_w_log(model, x):
    K = len(model.init_probs)
    N = len(x)
    w = make_table(K, N)
    w[:,:] = float('-inf')
    # Base case: fill out w[i][0] for i = 0..k-1
    for i in range(0, K-1): #range(K-1)
        w[i, 0] = model.init_probs[i]*model.emission_probs[i, x[0]]
    # Inductive case: fill out w[i][j] for i = 0..k, j = 0..n-1
    for n in range(1, N):
        for k in range(K):
            for j in range(K):
                w[k, n] = np.max([w[k, n], log(model.emission_probs[k,x[n]])+w[j, n-1]+log(model.trans_probs[j,k])])
    return w

def opt_path_prob_log(w):
    return np.max(w[:, -1])

w_short_l = compute_w_log(hmm_7_state, x_short_trans)
w_long_l = compute_w_log(hmm_7_state, x_long_trans)

print("\n (no log) opt_path_prob(w_short_l) =", opt_path_prob_log(w_short_nl))
print(" (no log) opt_path_prob(w_long_l) =", opt_path_prob_log(w_long_nl))

def backtrack_log(model, x, w):
    K = len(model.init_probs)
    N = len(x)
    z = np.zeros(N, dtype=int)
    
    z[-1] = np.argmax(w[:, -1])
    # taking argmax relative to columns (k)
    for n in range(N-2, -1, -1):
        z[n] = np.argmax(log(model.emission_probs[z[n+1], x[n+1]]) + w[:, n] + log(model.trans_probs[:, z[n+1]]))
    return z

z_short_viterbi_l = backtrack_log(hmm_7_state, x_short_trans, w_short_l)
z_long_viterbi_l = backtrack_log(hmm_7_state, x_long_trans, w_long_l)

#print("z_short_viterbi_l =", z_short_viterbi_l)
#print("z_long_viterbi_l =", z_long_viterbi_l)

### testing if implementation holds...

# Check that opt_path_prob is equal to joint_prob(hmm_7_state, x_short, z_viterbi)
print("opt_path_prob(w_short_nl) == joint_prob(hmm_7_state, x_short_trans, z_short_viterbi) :", 
       opt_path_prob(w_short_nl) == joint_prob(hmm_7_state, x_short_trans, z_short_viterbi))
print( opt_path_prob(w_short_nl),   joint_prob(hmm_7_state, x_short_trans, z_short_viterbi))

# Check that opt_path_prob_log is equal to joint_prob_log(hmm_7_state, x_short, z_viterbi_log)
print("opt_path_prob(w_short_l) == joint_prob_log(hmm_7_state, x_short_trans, z_short_viterbi_l) :", 
       opt_path_prob_log(w_short_l) == joint_prob_log(hmm_7_state, x_short_trans, z_short_viterbi_l))
print( opt_path_prob_log(w_short_l),   joint_prob_log(hmm_7_state, x_short_trans, z_short_viterbi_l))

# Do the above checks for x_long ...
print("opt_path_prob(w_long_nl) == joint_prob(hmm_7_state, x_long_trans, z_long_viterbi) :", 
       opt_path_prob(w_long_nl) == joint_prob(hmm_7_state, x_long_trans, z_long_viterbi))
print( opt_path_prob(w_long_nl),   joint_prob(hmm_7_state, x_long_trans, z_long_viterbi))

print("opt_path_prob(w_long_l) == joint_prob_log(hmm_7_state, x_long_trans, z_long_viterbi_l) :", 
       opt_path_prob_log(w_long_l) == joint_prob_log(hmm_7_state, x_long_trans, z_long_viterbi_l))
print( opt_path_prob_log(w_long_l),   joint_prob_log(hmm_7_state, x_long_trans, z_long_viterbi_l))


for i in range(100, len(x_long), 100):
    x = x_long[:i]
    z = z_long[:i]
    
    x_trans = translate_observations_to_indices(x)
    z_trans = translate_path_to_indices(z)
    
    # Make your experiment here...
    jpnl = joint_prob(hmm_7_state, x_trans, z_trans)
    jpl = joint_prob_log(hmm_7_state, x_trans, z_trans)
    print("i =", i)
    print("jpnl :", jpnl)
    print("jpl =", jpl)







